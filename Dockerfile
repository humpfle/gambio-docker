FROM php:8.1.17-apache

# Make directory accessible (only for develop)
RUN sed -i 's/Require all denied/Allow from all/g' /etc/apache2/apache2.conf
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf
RUN service apache2 restart


# install required modules
RUN apt-get update && apt-get install -y libfreetype6-dev \
                                         libjpeg62-turbo-dev \
                                         libmcrypt-dev \
                                         libicu-dev \
                                         libxml2-dev \
                                         locales \
                                         dos2unix \
                                         vim

RUN docker-php-ext-install mysqli pdo pdo_mysql soap gd && docker-php-ext-enable mysqli
RUN docker-php-ext-configure intl && docker-php-ext-install intl
RUN docker-php-ext-configure gd --with-jpeg
RUN docker-php-ext-install -j$(nproc) gd

# Set the locale
RUN sed -i -e 's/# de_DE.UTF-8 UTF-8/de_DE.UTF-8 UTF-8/' /etc/locale.gen && locale-gen
ENV LANG de_DE.UTF-8
ENV LANGUAGE de_DE:de
ENV LC_ALL de_DE.UTF-8

# Configure php / use the default production configuration
# TODO: verify that sed commands work. php.ini should contain new values. Script is not working anymore if defaults would change
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
RUN sed -i 's/memory_limit = 128M/memory_limit = 512M/g' "$PHP_INI_DIR/php.ini"
RUN sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 500M/g' "$PHP_INI_DIR/php.ini"
RUN sed -i 's/post_max_size = 8M/post_max_size = 500M/g' "$PHP_INI_DIR/php.ini"


# Copy shop
COPY ./shop/Shopsystem/Dateien /var/www/html
COPY ./shop/PdfCreator* /var/www/html
COPY ./shop/ResponsiveFilemanager* /var/www/html

# Make accessible
RUN chmod -R 777 /var/www/html
