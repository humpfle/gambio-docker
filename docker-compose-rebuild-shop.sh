#!/usr/bin/env bash

docker-compose rm -f -s -v gambio && docker-compose up -d --no-deps --build --force-recreate gambio
