# gambio-docker

## Preparation

* Download Gambio Shopsystem 4.8.0
* Unzip to folder `/shop`

Required Directory structure
* shop/Shopsystem/Dateien (mandatory)
* shop/PdfCreator (optional)
* shop/ResponsiveFilemanager (optional)

## Installation

* Run script `./docker-compose-down-up.sh` 
* Open url: `127.0.0.1/gambio_installer` 
* Configure database (name: `db`, user: `root`, password: `root`, database: `gambio`)  
* Fill out all mandatory fields
* Remove gambio_installer from container
* Done! (Application runs on host http://localhost (port 80))


## Base images documentation

* https://hub.docker.com/_/php
* https://hub.docker.com/_/mysql

